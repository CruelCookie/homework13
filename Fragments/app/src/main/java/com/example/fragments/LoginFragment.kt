package com.example.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.navigation.Navigation
import com.example.fragments.databinding.FragmentLoginBinding
import com.google.android.material.bottomnavigation.BottomNavigationMenuView

class LoginFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = FragmentLoginBinding.inflate(inflater)
        binding.bottomNavig.isVisible = false;
        binding.tvLogin.setOnClickListener{
            Navigation.findNavController(binding.root).navigate(R.id.nav_logFrag_to_regFrag)
        }
        binding.btLogin.setOnClickListener{
            Navigation.findNavController(binding.root).navigate(R.id.nav_logFrag_to_homeFrag)
        }
        return binding.root
    }

    companion object {
        @JvmStatic
        fun newInstance() = LoginFragment()
    }
}