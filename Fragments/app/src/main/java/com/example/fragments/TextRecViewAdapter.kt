package com.example.fragments

import android.os.Build
import android.view.*
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.example.fragments.databinding.TextrecItemBinding
import kotlin.random.Random

class TextRecViewAdapter: RecyclerView.Adapter<TextRecViewAdapter.textRecHolder>() {
    val textRecList = ArrayList<TextRec>()

    class textRecHolder(item: View): RecyclerView.ViewHolder(item) {
        val binding =TextrecItemBinding.bind(item)

        @RequiresApi(Build.VERSION_CODES.M)
        fun bind(textRec: TextRec) = with(binding){
            tvView.text = textRec.text
            tvView.setTextAppearance(textRec.style)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): textRecHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.textrec_item, parent, false)
        return textRecHolder(view)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: textRecHolder, position: Int) {
        for(i in textRecList){
            when(Random.nextInt(1,20)%3){
                0 -> i.style = R.style.MyTextView1
                1 -> i.style = R.style.MyTextView2
                2 -> i.style = R.style.MyTextView3
            }
        }
        holder.bind(textRecList[position])
    }

    override fun getItemCount(): Int {
        return textRecList.size
    }

    fun addTextRec(textRec: TextRec){
        textRecList.add(textRec)
        notifyDataSetChanged()
    }

    fun reStyle1(){
        notifyDataSetChanged()
    }
}